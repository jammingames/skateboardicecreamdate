﻿using UnityEngine;
using System.Collections;

public class AngularDisplacementRotation : MonoBehaviour
{

		float distanceTravelled = 0;
		public float distancePerFrame = 0;
		Vector3 lastPosition;
	
		void Start ()
		{
				lastPosition = transform.position;
		}
	
		void Update ()
		{
				distanceTravelled += Vector3.Distance (transform.position, lastPosition);
				distancePerFrame = transform.position.x - lastPosition.x;
				lastPosition = transform.position;
				float circumference = 2 * Mathf.PI * transform.collider2D.bounds.extents.y;
				float rotation = distancePerFrame / circumference;
				if (rotation > 0)
						transform.Rotate (Vector3.forward, -rotation);
				else if (rotation < 0)
						transform.Rotate (-Vector3.forward, -rotation);
				//print (distancePerFrame + "    " + rotation);
		}
}
