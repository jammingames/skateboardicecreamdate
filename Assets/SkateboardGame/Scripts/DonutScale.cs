﻿using UnityEngine;
using System.Collections;

public class DonutScale : MonoBehaviour
{


		public float scaleFactor = 1.2f;
		public EaseType easer;
		public float duration = 0.2f;
		public float scaleLimit;
		Vector3 origScale;
		// Use this for initialization
		void Start ()
		{
				origScale = transform.localScale;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.T))
						ScaleUp (scaleFactor);
				if (Input.GetKeyDown (KeyCode.R))
						ScaleDown (scaleFactor);
				if (Input.GetKeyDown (KeyCode.Y))
						ResetScale ();
		}

	
		public void ScaleUp (float scaleAmt)
		{
				if (transform.localScale.y < scaleLimit)
						StartCoroutine (transform.ScaleTo (transform.localScale * scaleAmt, duration, easer, null));
		}
	
		public void ScaleDown (float scaleAmt)
		{
				if (transform.localScale.y > origScale.y)
						StartCoroutine (transform.ScaleTo (transform.localScale / scaleAmt, duration, easer, null));
		}
	
		public void ResetScale ()
		{
				StartCoroutine (transform.ScaleTo (origScale, duration, easer, null));
		}

}
