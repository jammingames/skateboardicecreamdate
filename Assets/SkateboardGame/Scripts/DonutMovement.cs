﻿using UnityEngine;
using System.Collections;

public class DonutMovement : MonoBehaviour
{


		public float force = 1.0f;
		public float jumpForce = 10.0f;
		public float rayDistance = 1.0f;
		
		
		Vector2 forceVector;

		public bool isGrounded = true;
		// Use this for initialization
		void Start ()
		{
				
		}

		void FakeMovement ()
		{
				if (Input.GetAxis ("Horizontal") != 0)
						transform.Translate (new Vector3 (Input.GetAxis ("Horizontal") * force, transform.position.y, transform.position.z) * Time.deltaTime);
		}

		//public LayerMask mask;
		public LayerMask mask;
		public float skinOffset = 0.3f;
		bool CheckGroundCollision ()
		{
				//100 is arbitrary distance
				RaycastHit2D hit = Physics2D.Raycast (collider2D.bounds.center, -Vector2.up, 100, mask);
				if (hit.collider != null) {
						float distance = Mathf.Abs (hit.point.y - transform.position.y);
						if (distance < collider2D.bounds.extents.y + skinOffset)
								return true;
						else
								return false;
				}
				return false;
		}

		void RealMovement ()
		{
		
				if (Input.GetAxis ("Horizontal") != 0) {
						forceVector = Vector2.right * force * Input.GetAxis ("Horizontal");
				} else
						forceVector = Vector2.zero;
		
				RaycastHit2D hit = Physics2D.Raycast (collider2D.bounds.center, -Vector2.up, 100, mask);
				if (hit.collider != null) {
						float distance = Mathf.Abs (hit.point.y - transform.position.y);
						if (distance < collider2D.bounds.extents.y + skinOffset)
								isGrounded = true;
						else {
								isGrounded = false;
						}
				}
		
				if (Input.GetButtonDown ("Jump") && isGrounded) {
						isGrounded = false;
						rigidbody2D.AddForce (Vector2.up * jumpForce, ForceMode2D.Impulse);
				}

		}
		
		void Update ()
		{
				//		FakeMovement ();
				
				RealMovement ();

				
				
		}


		// Update is called once per frame
		void FixedUpdate ()
		{
				if (forceVector != Vector2.zero)
						rigidbody2D.AddForce (forceVector, ForceMode2D.Force);
				else if (isGrounded == true && Mathf.Abs (rigidbody2D.velocity.x) > 0.05f)
						rigidbody2D.AddForce (-rigidbody2D.velocity * force);
		}
}
