﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySampleAssets._2D;

public class CreateSkateboard : MonoBehaviour
{

		Vector3 pos;
		public float offset;
		public Transform skateboardPos;
		public GameObject particleEffect;
		List<SpriteRenderer> skateboardSprite;
		public Sprite spr;
		public AudioClip clip1, clip2;
		float originalCenter;
		// Use this for initialization
		
		void Awake ()
		{
				GameManager.instance.OnStateChange += HandleOnStateChange;
		}

		void HandleOnStateChange ()
		{
				if (GameManager.instance.gameState == GameState.Partner) {
						StartCoroutine (GetOffSkateboard (4.5f));
						print ("partner");
				} else if (GameManager.instance.gameState == GameState.IceCream) {
						StartCoroutine (GetOffSkateboard (5.5f));
						print ("icecream");
				}
		}
	
		void Start ()
		{
				
				//skateboardSprite.add() = skateboardPos.GetComponents<SpriteRenderer> ();
				skateboardSprite = new List<SpriteRenderer> ();
				skateboardSprite.Add (skateboardPos.GetComponent<SpriteRenderer> ());
				skateboardSprite.AddRange (skateboardPos.GetComponentsInChildren<SpriteRenderer> ());
				//skateboardSprite = skateboardSprite + skateboardPos.GetComponentsInChildren<SpriteRenderer> ();
				foreach (SpriteRenderer spr in skateboardSprite) {
						spr.enabled = false;
				}
		}

		public IEnumerator GetOffSkateboard (float delay)
		{
				DroppedSkateboard ();
				yield return new WaitForSeconds (delay);
				PickedUpSkateboard ();
		}

		public void DroppedSkateboard ()
		{
				particleSystem.Play ();
				if (clip1 != null)
						AudioSource.PlayClipAtPoint (clip1, transform.position);
				//var effect = GameObject.Instantiate (particleEffect, transform.position, Quaternion.identity) as GameObject;
				if (gameObject.GetComponent<PlatformerCharacter2D> ()) {

						var animator = gameObject.GetComponent<PlatformerCharacter2D> ();
						animator.canAnimate = true;
				}
				//		animator.anim.SetFloat ("vSpeed", 0);
				//		animator.anim.SetFloat ("Speed", 0);
		
				//	transform.position = new Vector3 (transform.position.x, transform.position.y - offset, transform.position.z);
				CircleCollider2D col = GetComponent<CircleCollider2D> ();
				col.center = new Vector2 (col.center.x, originalCenter);
				foreach (SpriteRenderer spr in skateboardSprite) {
						spr.enabled = false;
				}
				//skateboardSprite.sprite = spr;
				//skateboardSprite.enabled = true;
		}


		public void PickedUpSkateboard ()
		{
				particleSystem.Play ();
				if (clip2 != null)
						AudioSource.PlayClipAtPoint (clip2, transform.position);
				//var effect = GameObject.Instantiate (particleEffect, transform.position, Quaternion.identity) as GameObject;
		
				if (gameObject.GetComponent<PlatformerCharacter2D> ()) {
			

						var animator = gameObject.GetComponent<PlatformerCharacter2D> ();
						animator.canAnimate = false;
//						animator.anim.SetFloat ("vSpeed", 0);
//						animator.anim.SetFloat ("Speed", 0);
				}

				transform.position = new Vector3 (transform.position.x, transform.position.y + offset, transform.position.z);
				CircleCollider2D col = GetComponent<CircleCollider2D> ();
				originalCenter = col.center.y;
				col.center = new Vector2 (col.center.x, -offset);
				foreach (SpriteRenderer spr in skateboardSprite) {
						spr.enabled = true;
				}
				//skateboardSprite.sprite = spr;
				//skateboardSprite.enabled = true;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
