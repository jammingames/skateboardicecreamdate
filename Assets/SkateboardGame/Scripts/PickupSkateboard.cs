﻿using UnityEngine;
using System.Collections;

public class PickupSkateboard : MonoBehaviour
{


		public GameObject particleEffect;
		public string messageToSend;
		


		void OnTriggerEnter2D (Collider2D col)
		{
				if (col.tag == "Player") {

						var effect = GameObject.Instantiate (particleEffect, transform.position, Quaternion.identity) as GameObject;
						col.gameObject.SendMessage (messageToSend, gameObject, SendMessageOptions.DontRequireReceiver);
						GameObject.Destroy (gameObject, 0.2f);
						GameObject.Destroy (effect, 3);
				}
		}




}
