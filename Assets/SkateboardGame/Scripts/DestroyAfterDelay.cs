﻿using UnityEngine;
using System.Collections;

public class DestroyAfterDelay : MonoBehaviour
{


		public float delay = 0.3f;
		// Use this for initialization
		void Start ()
		{
				Destroy (gameObject, delay);
		}
	
		// Update is called once per frame
		void Update ()
		{
		
		}
}
