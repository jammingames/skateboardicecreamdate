﻿using UnityEngine;
using System.Collections;

public class LerpColor : MonoBehaviour
{

	
		public Color startColor;
		public Color endColor;
		public float duration = 5;
	
		float t = 0;
	
		// Use this for initialization
		void Start ()
		{
				GameManager.instance.OnStateChange += HandleOnStateChange;
				startColor = GetComponent<SpriteRenderer> ().color;
		}

		void HandleOnStateChange ()
		{
				if (GameManager.instance.gameState == GameState.IceCream)
						StartCoroutine (LerpColors ());
		}

		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.F))
						StartCoroutine (LerpColors ());
		}
	


		public IEnumerator LerpColors ()
		{
				yield return new WaitForSeconds (5);
				while (t < 1) {

						renderer.material.color = Color.Lerp (startColor, endColor, t);
						if (t < 1) { // while t below the end limit...
								// increment it at the desired rate every update:
								t += Time.deltaTime / duration;
						}
						yield return null;
				}
		}
}
