﻿using UnityEngine;
using System.Collections;

public class UfoAbduction : MonoBehaviour
{

		public Transform player;
		Transform partner;
		public LineRenderer line;
		public Material mat;
		public Color colorStart, colorEnd;
		public float lineStart, lineEnd;
		public AudioClip audioClip1, audioClip2;
		
		

		// Use this for initialization
		void Start ()
		{
				partner = GameManager.instance.partnerFollow.transform;
				StartCoroutine (transform.MoveTo (new Vector3 (player.position.x, player.position.y + 5, player.position.z), 3.0f, EaseType.QuadInOut, () =>
				{
						var newLine = createLine (new Vector3 (transform.position.x, transform.position.y, transform.position.z + 1), new Vector3 (transform.position.x, transform.position.y - 1, transform.position.z + 1), lineStart, lineEnd);
						StartCoroutine (newLine.MoveTo (new Vector3 (player.position.x, player.position.y, player.position.z), 2.0f, EaseType.Linear, () => {
								AudioSource.PlayClipAtPoint (audioClip1, transform.position);
								StartCoroutine (partner.ScaleTo (Vector3.zero, 2.0f, EaseType.ElasticInOut, null));
								StartCoroutine (partner.MoveTo (transform.position, 2.0f, EaseType.Linear, null));
								StartCoroutine (player.ScaleTo (Vector3.zero, 2.0f, EaseType.ElasticInOut, null));
								StartCoroutine (player.MoveTo (transform.position, 2.0f, EaseType.Linear, () => {
										StartCoroutine (newLine.MoveTo (transform.position, 2.0f, EaseType.Linear, () => {
												GameObject.Destroy (newLine);
												AudioSource.PlayClipAtPoint (audioClip2, transform.position);
												//GetComponent<AudioSource> ().play
												StartCoroutine (transform.MoveTo (new Vector3 (50, 200, 0), 2.0f, EaseType.QuadIn, () => {
														GameManager.instance.SetGameState (GameState.End);}));
										}));
								}));
						}));

				}));
		}

	
		
	
		private AwesomeLineRenderer createLine (Vector3 start, Vector3 end, float lineSize, float lineEnd)
		{	
				GameObject canvas = new GameObject ("line"); 
				canvas.transform.parent = transform;
				canvas.transform.rotation = transform.rotation;	
				//var newLine = (LineRenderer)canvas.AddComponent<LineRenderer> ();
				AwesomeLineRenderer lr = (AwesomeLineRenderer)canvas.AddComponent<AwesomeLineRenderer> ();
				lr.CreateNew (mat, colorStart, colorEnd, lineStart, lineEnd, start, end);
				return lr;
		}
}
