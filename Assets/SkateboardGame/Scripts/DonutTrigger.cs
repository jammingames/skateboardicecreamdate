﻿using UnityEngine;
using System.Collections;

public class DonutTrigger : MonoBehaviour
{

		const string MESSAGETOSEND = "ScaleUp";
		public float scaleAmmount = 1.2f;
		
		public GameObject explosionPrefab;

		public bool useTrigger = true;
		// Use this for initialization
		void Start ()
		{	
				
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void OnCollisionEnter2D (Collision2D other)
		{
				if (other.collider.tag == "Player" && !useTrigger) {
						//other.GetComponent<DonutMovement> ().ScaleUp ();
						Instantiate (explosionPrefab, transform.position, Quaternion.identity);
						other.collider.SendMessage (MESSAGETOSEND, scaleAmmount, SendMessageOptions.DontRequireReceiver);
						Destroy (gameObject);
						print ("YOU DUN DID IT");
				}
		}

		void OnTriggerEnter2D (Collider2D other)
		{
				if (other.tag == "Player" && useTrigger) {
						//other.GetComponent<DonutMovement> ().ScaleUp ();
						Instantiate (explosionPrefab, transform.position, Quaternion.identity);
						other.SendMessage (MESSAGETOSEND, scaleAmmount, SendMessageOptions.DontRequireReceiver);
						Destroy (gameObject);
						print ("YOU DUN DID IT");
				}
		}

}
