﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{
	
		// Link to the animated sprite
		//
	
		// State variable to see if the character is walking.
		private bool walking = false;

		private float origSpeed;
		private Rigidbody2D _rigidbody;

		public GameObject idleSpr, walkSpr, armSpr;



		void Awake ()
		{
				GameManager.instance.OnStateChange += HandleOnStateChange;
		}

		void HandleOnStateChange ()
		{
				if (GameManager.instance.gameState == GameState.IceCream) {
						StartCoroutine (IceCream ());
				}
		}

		void Start ()
		{
				// This script must be attached to the sprite to work.
				walkSpr.SetActive (false);
				idleSpr.SetActive (true);
				armSpr.SetActive (false);
				//transform.root.gameObject.BroadcastMessage ("SetAnim", this, SendMessageOptions.DontRequireReceiver);
				_rigidbody = transform.root.gameObject.rigidbody2D;
				origSpeed = 4;
				print (origSpeed);
		}


		IEnumerator IceCream ()
		{
				yield return new WaitForSeconds (3);
				idleSpr.SetActive (false);
				walkSpr.SetActive (false);
				armSpr.SetActive (true);
		}
	
		void FixedUpdate ()
		{
		
				if (GameManager.instance.gameState != GameState.IceCream) {

						if (Mathf.Abs (_rigidbody.velocity.x) < 0.1f)
								IdleAnim ();
						else if (Mathf.Abs (_rigidbody.velocity.x) > 0.1f)
								WalkAnim ();
				}
		}

		public void IdleAnim ()
		{
				if (!idleSpr.activeSelf) {
						walkSpr.SetActive (false);
						idleSpr.SetActive (true);
						//	anim.Play (idleAnim);
						// We dont have any reason for detecting when it completes
						//	anim.AnimationCompleted = null;
						walking = false;
				}
		}
		public void WalkAnim ()
		{
				if (!walkSpr.activeSelf) {
						idleSpr.SetActive (false);
						walkSpr.SetActive (true);
						// Walk is a looping animation
						// A looping animation never completes...
			
						walking = true;
				}
		}
}