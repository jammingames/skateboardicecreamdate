﻿using UnityEngine;
using System.Collections;

public class BounceCoroutine : MonoBehaviour
{

		// Use this for initialization

		public float scaleFactor;
		public float duration;
		public EaseType easer;
		Vector3 origScale;
		void Start ()
		{
				origScale = transform.localScale;
				StartCoroutine (transform.ScaleTo (transform.localScale * scaleFactor, duration, easer, BounceDown));
		}

		void BounceUp ()
		{
				StartCoroutine (transform.ScaleTo (transform.localScale * scaleFactor, duration, easer, BounceDown));
		}
		void BounceDown ()
		{
				StartCoroutine (transform.ScaleTo (origScale, duration, easer, BounceUp));
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
