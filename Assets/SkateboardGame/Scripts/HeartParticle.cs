﻿using UnityEngine;
using System.Collections;

public class HeartParticle : MonoBehaviour
{

		void Awake ()
		{
				GameManager.instance.OnStateChange += HandleOnStateChange;
		}
	
		void HandleOnStateChange ()
		{

				if (GameManager.instance.gameState == GameState.IceCream) {
						StartCoroutine (Hearticles ());
				}
		}


		IEnumerator Hearticles ()
		{
				particleSystem.Play ();
				yield return new WaitForSeconds (6);
				particleSystem.Stop ();
		}
}
