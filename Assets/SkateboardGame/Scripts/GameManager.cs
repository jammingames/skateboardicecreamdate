﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySampleAssets._2D;
using UnityEngine.UI;


public enum GameState
{
		NullState,
		Intro,
		MainMenu,
		Game,
		Partner,
		IceCream,
		UFO,
		End
}

public class GameManager : MonoBehaviour
{

		public SideScrollCameraFollow camFollow;
		public PlatformerCharacter2D playerController;
		Platformer2DUserControl plrController;
		public RunFromPlayer partnerFollow;
		GameObject partnerToSpawn;
		public GameObject[] characterPrefabs;
		public List<Sprite> characterSprites;
		public Image characterSelect;
		public Transform iceCream, partnerSpawn;
		public GameObject menuPanel, endPanel, fireWorks, UFO, chatBubble, chatBubblePlayer, iceCreamObject, partnerIceCream;
		public List<Transform> targets;
		int index;
		
		int spriteIndex = 0;

		

		bool isMoving = false;
		public System.Action OnComplete;

		public GameState gameState { get; private set; }
		public delegate void OnStateChangeHandler ();
		public event OnStateChangeHandler OnStateChange;
	
		public void SetGameState (GameState gameState)
		{
				this.gameState = gameState;
				if (OnStateChange != null) {
						OnStateChange ();
				}
		}

		void Awake ()
		{
				plrController = playerController.GetComponent<Platformer2DUserControl> ();
				OnStateChange += HandleOnStateChange;
				Debug.Log ("Current game state when Awakes: " + gameState);
				SetGameState (GameState.Intro);
				characterSelect.sprite = characterSprites [0];
				characterSelect.SetNativeSize ();
				partnerToSpawn = characterPrefabs [0];
		}

		public void SelectSprite (bool isNext)
		{
				if (isNext) {

						if (spriteIndex >= characterSprites.Count - 1) {
								spriteIndex = 0;
						} else {
								spriteIndex++;
						}
				} else {
						if (spriteIndex == 0) {
								spriteIndex = characterSprites.Count - 1;
						} else {
								spriteIndex--;
						}
				}
				characterSelect.sprite = characterSprites [spriteIndex];
				characterSelect.SetNativeSize ();
				partnerToSpawn = characterPrefabs [spriteIndex];
				//camFollow.ChangeTarget (targets [index], 1.0f, gameObject);
		}

		// Use this for initialization
		void Start ()
		{
				index = 0;
				plrController.enabled = false;
				playerController.rigidbody2D.velocity = Vector2.zero;
				camFollow.target = targets [index];
		}


		public void HandleOnStateChange ()
		{
				if (gameState == GameState.Intro) {
						print ("WELCOME TO GAEM");
						//	SetGameState (GameState.MainMenu);
				} else if (gameState == GameState.MainMenu) {
						menuPanel.SetActive (true);
						
				} else if (gameState == GameState.Game) {
						plrController.enabled = true;
						ChangeCamTarget (playerController.transform);
				} else if (gameState == GameState.IceCream) {
						plrController.enabled = false;
						playerController.rigidbody2D.velocity = Vector2.zero;
						ChangeCamTarget (iceCream.transform);
						IceCreamAchieved ();
						StartCoroutine (AbductPlayer (15));
						//instantiate icecream in both players hands.
						//at end of sequence, change sky and add fireworks.
						//change music
						//at end of timer change to ufo state
				} else if (gameState == GameState.Partner) {
						plrController.enabled = false;
						playerController.rigidbody2D.velocity = Vector2.zero;
						OnComplete = GetIceCream;
						ChangeCamTarget (partnerFollow.transform);
						//pop up Heart, and ice cream sequence;
						//at end of sequence, make friend follow.
				} else if (gameState == GameState.UFO) {
						plrController.enabled = false;
						playerController.rigidbody2D.velocity = Vector2.zero;
						OnComplete = FollowUFO;
						camFollow.xOffset = 0;
						camFollow.yOffset = -1f;
						ChangeCamTarget ();
						
			
				} else if (gameState == GameState.End) {
						plrController.enabled = false;
						playerController.rigidbody2D.velocity = Vector2.zero;
						ChangeCamTarget ();
						endPanel.SetActive (true);
						Vector3 fireworksPos = targets [index].position;
						fireworksPos.y = -1;
						var obj = GameObject.Instantiate (fireWorks, fireworksPos, Quaternion.Euler (-90, 0, 0)) as GameObject;
						//obj.transform.rotation = Quaternion.Euler (-90, 0, 0);
				}
					
				Debug.Log ("Handling state change to: " + gameState);
		}
			
		IEnumerator AbductPlayer (float duration)
		{
				yield return new WaitForSeconds (duration);
				SetGameState (GameState.UFO);
		}

		public void GetIceCream ()
		{
				OnComplete = null;
				StartCoroutine (IceCreamStuff ());

		}

	
		IEnumerator IceCreamFireworks ()
		{
				//	Vector3 fireworksPos = iceCream.transform.position;
				//	fireworksPos.y = -5;
				//	var obj = GameObject.Instantiate (fireWorks, fireworksPos, Quaternion.Euler (-90, 0, 0)) as GameObject;

				yield return new WaitForSeconds (2);
				iceCreamObject.SetActive (true);
				iceCreamObject.GetComponent<AudioSource> ().Play ();
				partnerIceCream.SetActive (true);
				ChangeCamTarget (playerController.transform);
				yield return new WaitForSeconds (4);
				
				plrController.enabled = true;
		}
	
		public void IceCreamAchieved ()
		{
				OnComplete = null;
				StartCoroutine (IceCreamFireworks ());
		}
		
		IEnumerator IceCreamStuff ()
		{
				chatBubble.SetActive (true);
				yield return new WaitForSeconds (2);
				chatBubble.SetActive (false);
				chatBubblePlayer.SetActive (true);
				ChangeCamTarget (playerController.transform);
				yield return new WaitForSeconds (2);
				chatBubblePlayer.SetActive (false);
				plrController.enabled = true;
				partnerFollow.target = playerController.transform;
		}
		public void FollowUFO ()
		{
				targets [index].GetComponent<UfoAbduction> ().enabled = true;
				targets [index].GetComponent<UfoAbduction> ().player = playerController.gameObject.transform;
				OnComplete = null;
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (Input.GetKeyDown (KeyCode.R))
						ResetGame ();
				if (Input.GetKeyDown (KeyCode.Escape))
						Application.Quit ();

				if (gameState == GameState.Intro) {
						if (Input.anyKeyDown)
								SetGameState (GameState.MainMenu);
				} else if (gameState == GameState.MainMenu) {
						
				} else if (gameState == GameState.Game) {
						if (Input.GetKeyDown (KeyCode.T))
								SetGameState (GameState.UFO);
				} else if (gameState == GameState.IceCream) {

				} else if (gameState == GameState.End) {
			
				} else if (gameState == GameState.Partner) {
						
				}
				
				
		}

		public void StartGame ()
		{
				var go = GameObject.Instantiate (partnerToSpawn, partnerSpawn.position, partnerSpawn.rotation) as GameObject;
				partnerFollow = go.GetComponent<RunFromPlayer> ();
				SetGameState (GameState.Game);
				
		}

		public void ResetGame ()
		{
				SetGameState (GameState.Intro);
				Application.LoadLevel (Application.loadedLevel);
		
		}

		public void EndGame ()
		{
				SetGameState (GameState.End);
				
		}

		public void ChangeCamTarget ()
		{
				if (index >= targets.Count - 1) {
						index = 0;
				} else {
						index++;
				}
				camFollow.ChangeTarget (targets [index], 1.0f, gameObject);
		}

		public void DoneMoving ()
		{
				if (OnComplete != null)
						OnComplete ();
		}

	
		public void ChangeCamTarget (Transform target)
		{
				if (targets.Contains (target))
						index = targets.IndexOf (target);
				camFollow.ChangeTarget (target, 1.0f, gameObject);
		}
	
	
	
		static GameManager _instance = null;
		public static GameManager instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(GameManager)) as GameManager;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("GameManager");
										_instance = obj.AddComponent<GameManager> ();
								}
						}

						return _instance;
				}
		}


		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}
}
