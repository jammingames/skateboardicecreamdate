﻿using UnityEngine;
using System.Collections;

public class PartnerTrigger : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
	
		}

		void OnTriggerEnter2D (Collider2D other)
		{
				if (GameManager.instance.gameState == GameState.Game && other.tag == "Player") {
						GameManager.instance.SetGameState (GameState.Partner);
			
				}
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
