﻿using UnityEngine;
using System.Collections;

public class LerpCameraColor : MonoBehaviour
{
	
	
		public Color startColor;
		public Color endColor;
		public float duration = 5;
	
		float t = 0;
	
		// Use this for initialization
		void Start ()
		{
				GameManager.instance.OnStateChange += HandleOnStateChange;
				startColor = Camera.main.backgroundColor;
		}
	
		void HandleOnStateChange ()
		{
				if (GameManager.instance.gameState == GameState.IceCream)
						StartCoroutine (LerpColors ());
		}
	
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.F))
						StartCoroutine (LerpColors ());
		}
	
	
	
		public IEnumerator LerpColors ()
		{
				while (t < 1) {
			
						Camera.main.backgroundColor = Color.Lerp (startColor, endColor, t);
						if (t < 1) { // while t below the end limit...
								// increment it at the desired rate every update:
								t += Time.deltaTime / duration;
						}
						yield return null;
				}
		}
}
