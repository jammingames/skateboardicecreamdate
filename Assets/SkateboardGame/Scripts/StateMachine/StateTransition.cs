﻿using System;


class StateTransition<T> : IEquatable<StateTransition<T>>
{
		readonly T currentState;
		readonly T nextState;
	
		public StateTransition (T curr, T next)
		{
				this.currentState = curr;
				this.nextState = next;
		}
	
		public override int GetHashCode ()
		{
				// Overflow is ok, let it wrap!
				unchecked {
						int hash = (7 * 13 + currentState.GetHashCode ());
						hash = hash * 13 + nextState.GetHashCode ();
						return hash;
				}
		}
	
		public bool Equals (StateTransition<T> other)
		{
				if (ReferenceEquals (this, other))
						return true;
				return other != null &&
						this.currentState.Equals (other.currentState) &&
						this.nextState.Equals (other.nextState);
		}
	
		public override string ToString ()
		{
				return currentState.ToString () + " -> " + nextState.ToString ();
		}
}