﻿using System;
using System.Collections.Generic;


public class FiniteStateMachine<T>
{
		Dictionary<StateTransition<T>, TransitionActions> transitions;
		T currState;
		T prevState;
	
		public event EventHandler StateChanged;

		private class TransitionActions
		{
				readonly Action before;
				readonly Action after;
				readonly Action<EventArgs> announce;
		
				public TransitionActions (Action before, Action after, Action<EventArgs> announce)
				{
						this.before = before;
						this.after = after;
						this.announce = announce;
				}
		
				public Action Before {
						get { return this.before; }
				}
		
				public Action After {
						get { return this.after; }
				}
		
				public Action<EventArgs> Announce {
						get { return this.announce; }
				}
		}

	
	#region Getters and Setters
		public T CurrentState {
				get;
				private set;
		}
	
		public T PrevState {
				get;
				private set;
		}
	#endregion
	
		public FiniteStateMachine ()
		{
				this.transitions = new Dictionary<StateTransition<T>, TransitionActions> ();
		}

		public void ChangeState (T newState)
		{
				var t = new StateTransition<T> (this.CurrentState, newState);
				TransitionActions actions;
				// If this transition has been defined, proceed.
				if (this.transitions.TryGetValue (t, out actions)) {
						if (actions.Before != null)
								actions.Before ();
			
						this.PrevState = this.CurrentState;
						this.CurrentState = newState;
			
						// Notify listeners that state has changed.
						this.OnStateChanged (EventArgs.Empty);
			
						if (actions.Announce != null)
								actions.Announce (EventArgs.Empty);
			
						if (actions.After != null)
								actions.After ();
				} else {
						UnityEngine.Debug.Log ("[FSM] Warning! ChangeState: Transition Not Defined - " + t);
				}
		}
	
		virtual protected void OnStateChanged (EventArgs e)
		{
				// Ensure things are safer with multiple threads.
				var changed = StateChanged;
				if (changed != null)
						changed (this, e);
		}

		public void AddTransition (T s1, T s2, Action before, Action after,
	                          Action<EventArgs> announce)
		{
				var t = new StateTransition<T> (s1, s2);
				// If it already exists, don't add it again.
				if (transitions.ContainsKey (t)) {
						UnityEngine.Debug.Log ("[FSM] Warning! AddTransition: Transition Already Exists - "
								+ t);
						return;
				}
		
				this.transitions.Add (t, new TransitionActions (before, after, announce));
		}
	
		public void AddTransition (T s1, T s2, Action<EventArgs> announce)
		{
				this.AddTransition (s1, s2, null, null, announce);
		}
	
		public void DeleteTransition (T s1, T s2)
		{
				var t = new StateTransition<T> (s1, s2);
				if (!transitions.Remove (t)) {
						UnityEngine.Debug.Log ("[FSM] Warning! DeleteTransition: Transition not found - "
								+ t);
				}
		}


}
