﻿using UnityEngine;
using System.Collections;

public class SideScrollCameraFollow : MonoBehaviour
{
		public Transform target;
		public float xOffset;			//offset the x in relation to target (ie want to see more in front of character to the right)
		public float yOffset;
		public Transform leftBumper, rightBumper; //objects to grab bounds from
		public bool canTarget = true;
		public Transform _trans;
		void Awake ()
		{
				_trans = transform;
		}

		void LateUpdate ()
		{
				if (canTarget) {

						Vector3 newPosition = target.position; 
						newPosition.x = Mathf.Clamp (newPosition.x + xOffset, leftBumper.position.x, rightBumper.position.x);
						newPosition.y = Mathf.Clamp (newPosition.y + yOffset, leftBumper.position.y, rightBumper.position.y);
						newPosition.z = _trans.position.z;
						_trans.position = newPosition;
				}
		}

		public void ChangeTarget (Transform tar, float duration, GameObject obj)
		{
				canTarget = false;
				Vector3 newPosition = tar.position; 
				newPosition.x = Mathf.Clamp (newPosition.x + xOffset, leftBumper.position.x, rightBumper.position.x);
				newPosition.y = Mathf.Clamp (newPosition.y + yOffset, leftBumper.position.y, rightBumper.position.y);
				newPosition.z = _trans.position.z;

				
				
				StartCoroutine (transform.MoveTo (newPosition, duration, EaseType.QuadOut, () => {
						obj.SendMessage ("DoneMoving", SendMessageOptions.DontRequireReceiver);
						target = tar;
						canTarget = true;}));
		}
}
