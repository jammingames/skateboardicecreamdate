﻿using UnityEngine;
using System.Collections;

public class RunFromPlayer : MonoBehaviour
{

		//public Transform target;
		public float force;


		private Transform _target;
		public Transform target { get { return target; } set { _target = value; } }

		float targetDistance = 4.5f;
		float minDist = 1.2f;
		float distance;
		bool fwdMove = true;
		
		void Update ()
		{	
				if (_target) {
						fwdMove = _target.rigidbody2D.velocity.x > 0;
						distance = Vector3.Distance (transform.position, _target.position);
				}
		}

		void FixedUpdate ()
		{
				if (_target) {
						bool moveFwd = rigidbody2D.velocity.x > 0;
						if (distance > targetDistance) {
								Vector3 dir = _target.position - transform.position;
								rigidbody2D.AddForce (new Vector2 (dir.x, dir.y) * force);
						} else if (distance < targetDistance) {
								if (Mathf.Abs (rigidbody2D.velocity.x) > 0.1f && moveFwd == fwdMove && distance > minDist)
										rigidbody2D.velocity = _target.rigidbody2D.velocity;
						}
						
				}
		}
}

